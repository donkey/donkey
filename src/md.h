/*
 * md.h
 *		 Copyright (C) 1995 Kazuhiko Yamamoto
 *	      Kazuhiko Yamamoto <kazu@is.aist-nara.ac.jp>
 *		 Copyright (C) 2016 Peter Pentchev <roam@ringlet.net>
 */

#if defined(D_HAVE_MD5_OPENSSL)

#include <openssl/md4.h>
#include <openssl/md5.h>

#elif defined(D_HAVE_MD5_LIBMD)

#include <md4.h>
#include <md5.h>

#endif

#define D_MD4 0
#define D_MD5 1
/* see mdsw in md.c */

extern int MD;

typedef union {
	MD4_CTX	md4;
	MD5_CTX	md5;
} D_MDX_CTX;

/*
 * MD
 */

struct mdsw {
	void (*md_init)(D_MDX_CTX *);
	void (*md_update)(D_MDX_CTX *, const UINT1 *, size_t);
	void (*md_final)(UINT1 [16], D_MDX_CTX *);
};


/*
 * MD Switch
 */

extern const struct mdsw mdsw[];
