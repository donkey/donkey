/*
 * donkey.h
 */

/* should be included after config.h */

char *btoe(const char *md);
int passphrase(const char *prompt, char *phrase, size_t size, bool rej);
void secure_hash(char *x);
int keycrunch(char *result, const char *seed, const char *passwd);
