/*
 * md.c
 *		 Copyright (C) 1995 Kazuhiko Yamamoto
 *	      Kazuhiko Yamamoto <kazu@is.aist-nara.ac.jp>
 *		 Copyright (C) 2016, 2018 Peter Pentchev <roam@ringlet.net>
 */

#include "config.h"
#include "md.h"

#if defined(D_HAVE_MD5_OPENSSL)

#define d_MD4Init_(c)			MD4_Init(c)
#define d_MD4Update_(c, data, len)	MD4_Update(c, data, len)
#define	d_MD4Final_(digest, c)		MD4_Final(digest, c)

#define d_MD5Init_(c)			MD5_Init(c)
#define d_MD5Update_(c, data, len)	MD5_Update(c, data, len)
#define	d_MD5Final_(digest, c)		MD5_Final(digest, c)

#else

#define d_MD4Init_(c)			MD4Init(c)
#define d_MD4Update_(c, data, len)	MD4Update(c, data, len)
#define	d_MD4Final_(digest, c)		MD4Final(digest, c)

#define d_MD5Init_(c)			MD5Init(c)
#define d_MD5Update_(c, data, len)	MD5Update(c, data, len)
#define	d_MD5Final_(digest, c)		MD5Final(digest, c)

#endif

static void d_MD4Init(D_MDX_CTX *ctx)
{
	d_MD4Init_(&ctx->md4);
}

static void d_MD4Update(D_MDX_CTX *ctx, const UINT1 *data, size_t len)
{
	d_MD4Update_(&ctx->md4, data, len);
}

static void d_MD4Final(UINT1 digest[16], D_MDX_CTX *ctx)
{
	d_MD4Final_(digest, &ctx->md4);
}

static void d_MD5Init(D_MDX_CTX *ctx)
{
	d_MD5Init_(&ctx->md5);
}

static void d_MD5Update(D_MDX_CTX *ctx, const UINT1 *data, size_t len)
{
	d_MD5Update_(&ctx->md5, data, len);
}

static void d_MD5Final(UINT1 digest[16], D_MDX_CTX *ctx)
{
	d_MD5Final_(digest, &ctx->md5);
}

const struct mdsw mdsw[] = {
    {d_MD4Init, d_MD4Update, d_MD4Final},
    {d_MD5Init, d_MD5Update, d_MD5Final},
};
