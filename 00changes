<<<1.2.0 August 27, 2018>>>

* Add some missing empty lines in this changelog.
* Add support for running the test-donkey tests through tmux and
  make that the default; it may be overridden by specifying
  TEST_PROCESS_DRIVER='pty' in the environment.  This change also
  drops support for Python 2.x in the test-donkey tool.

<<<1.1.0 May 18, 2018>>>

* Regenerate the configure script.
* Add a test-donkey Python tool.
* Add the --features command-line option.
* Let --help and --version write to the standard output stream, not
  the standard error one.

<<<1.0.2 January 10, 2018>>>

* Remove a double "const" to fix a compiler warning.

<<<1.0.1 November 15, 2016>>>

* Correct a typographical error.

<<<1.0.0 April 18, 2016>>>

* Fix the wording of the 00readme file a bit.
* Point to my Ringlet webpage and GitLab repository in the 00readme file.
* Fix the wording of a comment in the donkey.c file.
* Fix a couple of compiler warnings.
* Properly honor CPPFLAGS, CFLAGS, and LDFLAGS from the environment.
* Update the src/configure.in template a bit:
  - add a newline to the last line
  - check for Large File Support
  - add -Werror to the compiler flags if requested by --with-werror
  - rename the template to src/configure.ac
* Add a donkey.1 manual page written by Fumitoshi UKAI <ukai@debian.or.jp>
  for the Debian GNU/Linux package of donkey.
* Rework the "install" target in the Makefile and also install
  the manual page.
* Use %H:%M:%S instead of the GNU %T strftime() extension.
* Add function prototypes in a couple of places that were missing them, and
  declare a signal handler's parameter as unused.
* Assume a reasonably recent build environment:
  - define _POSIX_C_SOURCE and _XOPEN_SOURCE
  - drop the embedded implementation of getopt() and getopt_long()
    (may need to add checks for external libraries in the future)
  - constify some variables and function parameters
  - drop the PROTO macro, ANSI C prototypes should be available everywhere
  - use "static" instead of "private" and drop the "public" definition
  - use some C99 features: the "bool" type, "const" in more places, and
    in-place variable declarations
* Break key_init() into a separate function in donkey.c and use a global
  variable directly in turnecho() in passphrase.c to avoid shadowed
  variables.
* Remove the unused tailor.h header file.
* Drop MD2 as a supported algorithm.
* Use an external library - either OpenSSL's libcrypto or libmd - for
  the MD4 and MD5 hash functions.
* Add my copyright notice to some of the source files.
	Peter Pentchev <roam@ringlet.net>

<<<0.5 October 4, 1995>>>

* Define get_sigint() to echo on when interrupted.
	Masaya Nakayama <nakayama@sakura.nc.u-tokyo.ac.jp>


<<<0.4 October 2, 1995>>>

* Display "Re-enter" instead of "Enter" for the second prompt when "-i".
* Reject short passphrase when "-i".
* Display min and max characters for passphrase.
* Allow sequence number 0.
* Allow stop and suspend.

Many thanks to
	Masaya Nakayama <nakayama@sakura.nc.u-tokyo.ac.jp>
	NAKAMURA Motonori <motonori@cs.ritsumei.ac.jp>


<<<0.3, April 29, 1995>>>

* FreeBSD.2.1.0, OSF/1 3.x and NEWS-OS 6.x.
* -i now requires user name.
* -i's messages are printed to stderr.
* MD2, MD4, and MD5 are integrated.
* Error message is improved.


<<<0.2, April 28, 1995>>>

* BIG bug: keycrunch was forgot when -i is on...
* gethostname() and ctime().
* HP-UX 9.x, HI-UX/WE2, NeXT, EWS4800 4.0, and NEWS-OS 4.2. 


<<<0.1, April 26, 1995>>>

* Initial implement.
* -i option.
* SunOS 4.1.x Solaris 2.x, OSF/1 2.x, ULTRIX 4.x, BSD/OS 2.0 and IRIX 5.x.
